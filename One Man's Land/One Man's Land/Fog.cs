﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;


namespace One_Man_s_Land
{
    public class Fog
    {

        Texture2D FogTexture1;
        Texture2D FogTexture2;
        Texture2D FogTexture3;
        Texture2D FogTexture4;

        Vector2 FogPosition1;
        Vector2 FogPosition2;
        Vector2 FogPosition3;
        Vector2 FogPosition4;
        
        
        public void Load(ContentManager content)
        {
            FogTexture1 = content.Load<Texture2D>("BackGrounds/Level01Fog01");
            FogTexture2 = content.Load<Texture2D>("BackGrounds/Level01Fog01");
            FogTexture3 = content.Load<Texture2D>("BackGrounds/Level01Fog02");
            FogTexture4 = content.Load<Texture2D>("BackGrounds/Level01Fog02");

            FogPosition1 = new Vector2(0);
            FogPosition2 = new Vector2(4000, 0);
            FogPosition3 = new Vector2(0, 0);
            FogPosition4 = new Vector2(4000, 0);
        }
                
        public void Update()
        {
            float backFogSpeed = 0.4f;
            float frontFogSpeed = 0.2f;

            FogPosition1.X -= backFogSpeed;
            FogPosition2.X -= backFogSpeed;
            FogPosition3.X -= frontFogSpeed;
            FogPosition4.X -= frontFogSpeed;

            if (FogPosition1.X <= -4000)
                FogPosition1.X = 4000;

            if (FogPosition2.X <= -4000)
                FogPosition2.X = 4000;
            
            if (FogPosition3.X <= -4000)
                FogPosition3.X = 4000;

            if (FogPosition4.X <= -4000)
                FogPosition4.X = 4000;
        }
        
        public void DrawBackFog(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(FogTexture1, FogPosition1, Color.White);
            spriteBatch.Draw(FogTexture2, FogPosition2, Color.White);

        }

        public void DrawFrontFog(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(FogTexture3, FogPosition3, Color.White);
            spriteBatch.Draw(FogTexture4, FogPosition4, Color.White);
        
        
        }


    }
}
