﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class Player
    {

        
        Texture2D PlayerSpreadSheet;
        
        public Vector2 PlayerPosition;
        public Vector2 PlayerVelocity;
               
        Rectangle BottomCollision;
        Rectangle SourceFrameToDraw;
        Rectangle JumpFrameToDraw;
        Rectangle PlayerRectangle;
        
        public string LastStanding;

        int CurrentFrameX;
        int CurrentFrameY;
        int CurrentJumpFrameX;

        KeyboardState oldKeyState;
        GamePadState oldPadState;

        const int PlayerWidth = 192;
        const int PlayerHeight = 256;

        float AnimationTimer;
        float AnimationInterval;
        float BreathInterval;
        float JumpSpeed;
        float LandingTimer;


        CharacterState player1State = new CharacterState();
        
        
        
        enum PlayerAnimationState { StandingRight, StandingLeft, WalkingRight, WalkingLeft, RunningRight, RunningLeft, JumpingRight, JumpingLeft, LandingRight, LandingLeft, CrouchRight, CrouchLeft };
        PlayerAnimationState playerAnimationState = new PlayerAnimationState();

        enum PlayerCollisionState { HitBottom, HitTop, HitRight, HitLeft, WalkRight, WalkLeft, NoHit };
        PlayerCollisionState playerCollisiontState = new PlayerCollisionState();

        enum JumpState { IsJumpingRight, IsJumpingLeft, IsLanding, NoJump }
        JumpState jumpState = new JumpState();

        public enum SpellState { ShieldOn, ShieldOff, ShieldIn, ShieldOut };
        public SpellState spellState = new SpellState();
        public bool Shield = false;
        public bool ShieldGrown = false;

        bool ControllerActive;
        bool hasjump;
        bool isCrouch;

        public Player()
        {
            
        }


        public void Initialize(GraphicsDevice graphicsDevice)
        {

            LastStanding = "Right";
                       
            PlayerPosition = new Vector2(700, 600);
            PlayerVelocity = new Vector2(0, 0);
            
            CurrentFrameX = 0;
            CurrentFrameY = 0;
            CurrentJumpFrameX = 0;
            AnimationInterval = 80f;
            BreathInterval = 120f;
            
            playerAnimationState = PlayerAnimationState.StandingRight;
            playerCollisiontState = PlayerCollisionState.NoHit;
            jumpState = JumpState.NoJump;
            JumpSpeed = -30;

            ControllerActive = true;
            hasjump = false;
            isCrouch = false;
        }


        public void Load(ContentManager content)
        { 
            
            
            PlayerSpreadSheet = content.Load<Texture2D>("Player/PlayerSpreadSheet");
            
                   
        }

        
        public void Update(GameTime gameTime)
        {

           
            PlayerPosition += PlayerVelocity;
            PlayerVelocity.X = 0;
            PlayerRectangle = new Rectangle((int)PlayerPosition.X, (int)PlayerPosition.Y + 50, 50, 200);
            
            if(LastStanding == "Right")
                playerAnimationState = PlayerAnimationState.StandingRight;

            if(LastStanding == "Left")
                playerAnimationState = PlayerAnimationState.StandingLeft;

            LoadSegment();

            GetControllerInput();
            Animation(gameTime);
            
            if (PlayerVelocity.Y < 10)
                PlayerVelocity.Y += 10;
               

                     
            

        }
        

        public void Draw(SpriteBatch spriteBatch)
        {


            if (hasjump == false) 
                spriteBatch.Draw(PlayerSpreadSheet, PlayerPosition, SourceFrameToDraw, Color.White);

            if (hasjump == true)
                spriteBatch.Draw(PlayerSpreadSheet, PlayerPosition, JumpFrameToDraw, Color.White);


                        
                
        }


        //void CollisionDetection()
        //{
        //    BottomCollision = new Rectangle((int)PlayerPosition.X + 86, (int)PlayerPosition.Y + 256, 20, 20); //Zone de détection de contact avec le sol

        //    if (playerCollisiontState == PlayerCollisionState.HitBottom)
        //        PlayerVelocity.Y = 0;

        //    if (playerCollisiontState == PlayerCollisionState.NoHit)
        //        PlayerVelocity.Y = 12;



        //    //if (BottomCollision.Intersects(RailRectangle1))
        //    //{
        //    //    PixelCollision(RailRectangle1, RailTexture);
        //    //    if (PixelCollision(RailRectangle1, RailTexture) == true)
        //    //    { playerCollisiontState = PlayerCollisionState.HitBottom; }
                
        //    //}

            


        //}
                
        
        void GetControllerInput()
        {
            
            KeyboardState keystate = Keyboard.GetState();
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            
            

            if (ControllerActive)
            {
                
                if ((keystate.IsKeyDown(Keys.Left)) || (gamePadState.DPad.Left == ButtonState.Pressed))
                {

                    if (!isCrouch)
                    {
                        PlayerVelocity.X = -10;
                        playerAnimationState = PlayerAnimationState.RunningLeft;
                        LastStanding = "Left";
                    }
                }


                if ((keystate.IsKeyDown(Keys.Right)) || (gamePadState.DPad.Right == ButtonState.Pressed))
                {

                    if (!isCrouch)
                    {
                        PlayerVelocity.X = 10;
                        playerAnimationState = PlayerAnimationState.RunningRight;
                        LastStanding = "Right";
                    }
                }

                if ((keystate.IsKeyDown(Keys.Down)) || (gamePadState.DPad.Down == ButtonState.Pressed))
                {
                    if (LastStanding == "Right")
                    {
                        playerAnimationState = PlayerAnimationState.CrouchRight;
                        isCrouch = true;
                    }

                    if (LastStanding == "Left")
                    {
                        playerAnimationState = PlayerAnimationState.CrouchLeft;
                        isCrouch = true;
                    }
                }
                else
                    isCrouch = false;


                if ((gamePadState.Triggers.Right != 0) && (oldPadState.Triggers.Right != 0))
                {

                    Shield = true;
                    if (!ShieldGrown)
                        spellState = SpellState.ShieldIn; //Le Shield en train de grandir
                    if (ShieldGrown)
                        spellState = SpellState.ShieldOn; //Le shield complètement formé
                }


                if (gamePadState.Triggers.Right == 0)
                {

                    ShieldGrown = false;
                    spellState = SpellState.ShieldOut; 
                
                }


                if (hasjump == false)
                {
                    if ((keystate.IsKeyDown(Keys.Space)) && ((oldKeyState.IsKeyUp(Keys.Space))))
                    {
                        if (LastStanding == "Right")
                        { jumpState = JumpState.IsJumpingRight; }
                        if (LastStanding == "Left")
                        { jumpState = JumpState.IsJumpingLeft; }

                    }
                    if ((gamePadState.Buttons.A == ButtonState.Pressed) && (oldPadState.Buttons.A == ButtonState.Released))
                    {
                    
                        if (LastStanding == "Right")
                        { jumpState = JumpState.IsJumpingRight; }
                        if (LastStanding == "Left")
                        { jumpState = JumpState.IsJumpingLeft; }
                    
                    }



                }

                

                if (jumpState == JumpState.IsJumpingRight)
                {
                    hasjump = true; 
                    playerAnimationState = PlayerAnimationState.JumpingRight;
                    PlayerPosition.Y += JumpSpeed;
                    JumpSpeed += 1;
                    
                }

                if (jumpState == JumpState.IsJumpingLeft)
                {
                    hasjump = true;
                    playerAnimationState = PlayerAnimationState.JumpingLeft;
                    PlayerPosition.Y += JumpSpeed;
                    JumpSpeed += 1;
                }


                oldKeyState = keystate;
                oldPadState = gamePadState;




                                

            }
            
         if (jumpState == JumpState.IsLanding)
            {
                hasjump = true;
                ControllerActive = false;
                if(LastStanding == "Right")  
                   playerAnimationState = PlayerAnimationState.LandingRight;
                if (LastStanding == "Left")
                    playerAnimationState = PlayerAnimationState.LandingLeft;
            }

        }

        
        void Animation(GameTime gameTime)
        {
                        
            SourceFrameToDraw = new Rectangle(CurrentFrameX * PlayerWidth, CurrentFrameY * PlayerHeight, PlayerWidth, PlayerHeight);
            JumpFrameToDraw = new Rectangle(CurrentJumpFrameX * PlayerWidth, CurrentFrameY * PlayerHeight, PlayerWidth, PlayerHeight);
            
            if (playerAnimationState == PlayerAnimationState.StandingRight)
            {
                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 0;

                if (AnimationTimer > BreathInterval)
                {
                    CurrentFrameX++;
                    AnimationTimer = 0;
                }

                if (CurrentFrameX == 11)
                {
                    CurrentFrameX = 0;

                }
            }

            if (playerAnimationState == PlayerAnimationState.StandingLeft)
            {
                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 1;

                if (AnimationTimer > BreathInterval)
                {
                    CurrentFrameX++;
                    AnimationTimer = 0;
                }

                if (CurrentFrameX == 11)
                {
                    CurrentFrameX = 0;

                }
            }

            if (playerAnimationState == PlayerAnimationState.RunningRight)
            {
                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 4;

                if (AnimationTimer > AnimationInterval)
                {
                    CurrentFrameX++;
                    AnimationTimer = 0;
                }
                
                if (CurrentFrameX == 11)
                {
                    CurrentFrameX = 0;

                }
                        
            }

            if (playerAnimationState == PlayerAnimationState.RunningLeft)
            {
                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 5;

                if (AnimationTimer > AnimationInterval)
                {
                    CurrentFrameX++;
                    AnimationTimer = 0;
                }

                if (CurrentFrameX == 11)
                {
                    CurrentFrameX = 0;

                }
            }

            if (playerAnimationState == PlayerAnimationState.JumpingRight)
            {

                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                float jumpInterval = 60f;
                CurrentFrameY = 6;

                if (AnimationTimer > jumpInterval)
                {
                    CurrentJumpFrameX++;
                    AnimationTimer = 0;
                }

                if (CurrentJumpFrameX > 5)
                {
                    CurrentJumpFrameX = 5;
                }

            }

            if (playerAnimationState == PlayerAnimationState.JumpingLeft)
            {
                AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                float jumpInterval = 60f;
                CurrentFrameY = 7;

                if (AnimationTimer > jumpInterval)
                {
                    CurrentJumpFrameX++;
                    AnimationTimer = 0;
                }

                if (CurrentJumpFrameX > 5)
                {
                    CurrentJumpFrameX = 5;
                }

            }

            if (playerAnimationState == PlayerAnimationState.LandingRight)
            {
                LandingTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 8;
                CurrentJumpFrameX = 0;

                if (LandingTimer > 200)
                {
                    LandingTimer = 0;
                    hasjump = false;
                    jumpState = JumpState.NoJump;
                    ControllerActive = true;

                }
                            
            }

            if (playerAnimationState == PlayerAnimationState.LandingLeft)
            {
                LandingTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                CurrentFrameY = 9;
                CurrentJumpFrameX = 0;
                
                if (LandingTimer > 200)
                {
                    LandingTimer = 0;
                    hasjump = false;
                    jumpState = JumpState.NoJump;
                    ControllerActive = true;

                }

            }

            if (playerAnimationState == PlayerAnimationState.CrouchRight)
            {
                CurrentFrameY = 8;
                CurrentFrameX = 0;
            
            }

            if (playerAnimationState == PlayerAnimationState.CrouchLeft)
            {
                CurrentFrameY = 9;
                CurrentFrameX = 0;
            }



        }

        
        void LoadSegment()
        {

            var CoordonneList = new List<Tuple<Vector2, Vector2>>
            {
                

                new Tuple<Vector2, Vector2>(new Vector2(0, 1200), new Vector2(430, 1170)),
                new Tuple<Vector2, Vector2>(new Vector2(430, 1170), new Vector2(810, 1150)),
                new Tuple<Vector2, Vector2>(new Vector2(810, 1150), new Vector2(1500, 1030)),
                new Tuple<Vector2, Vector2>(new Vector2(1500,1030), new Vector2(1800, 960)),
                new Tuple<Vector2, Vector2>(new Vector2(1800,960), new Vector2(2040, 910)),
                new Tuple<Vector2, Vector2>(new Vector2(2040,910), new Vector2(1930, 1110)),
                new Tuple<Vector2, Vector2>(new Vector2(1930, 1110), new Vector2(2020, 1140)),
                new Tuple<Vector2, Vector2>(new Vector2(2020, 1140), new Vector2(2900, 1125)),
                new Tuple<Vector2, Vector2>(new Vector2(290, 1125), new Vector2(3400, 1130)),
                new Tuple<Vector2, Vector2>(new Vector2(3400, 1130), new Vector2(3790, 1180)),
                new Tuple<Vector2, Vector2>(new Vector2(3790, 1180), new Vector2(4000, 1175)),
                

                        
            };

            Rectangle DecorRecangle1 = new Rectangle(0, 0, 0, 0);

            foreach (Tuple<Vector2, Vector2> Coordonne in CoordonneList)
            {

                Vector2 Start;
                Vector2 End;

                Start = Coordonne.Item1;
                End = Coordonne.Item2;

                LineCollision(Start, End, DecorRecangle1);

            }

        }


        void LineCollision(Vector2 StartSeg, Vector2 EndSeg, Rectangle Decor1)
        {

            Rectangle rectangleCollisionGauche = new Rectangle((int)PlayerPosition.X - 24, (int)PlayerPosition.Y + 56, 48, 100);  //petit rectangle à mi-hauteur du perso
            Rectangle rectangleCollisionDroite = new Rectangle((int)PlayerPosition.X + 216, (int)PlayerPosition.Y + 56, 48, 100); //petit rectangle à mi-hauteur du perso
            
                
            if ((PlayerPosition.X + 100 > StartSeg.X) && ((PlayerPosition.X + 60) < EndSeg.X))
            {
                //Y = mx + c
                
                float m = (EndSeg.Y - StartSeg.Y) / (EndSeg.X - StartSeg.X); // Pente
                float c = StartSeg.Y - (m * StartSeg.X); // Y Intercept



                //Résolution de l'équation
                if (((PlayerPosition.Y + 256) > (m * PlayerPosition.X + c)) && ((PlayerPosition.Y + 256) < (m * PlayerPosition.X + c) + 48)) //Une mince bande sous le rail dans laquelle la collision se résoud.  Comme ça, on peut passer sous une ligne.
                {
                    PlayerPosition.Y = ((m * PlayerPosition.X + c) - 240);


                    if (hasjump == true)
                    {
                        jumpState = JumpState.IsLanding; 
                                           
                    }


                    if (hasjump == false)
                    {
                        JumpSpeed = -30;
                        CurrentJumpFrameX = 0;
                    }
                }
            }

            if (PlayerRectangle.Intersects(Decor1))
                {
                    PlayerPosition.X += 1;
                }
            



        }

   }

}
