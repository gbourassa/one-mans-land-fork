﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace One_Man_s_Land
{
    class PixelPerfect
    {

        int CurrentFrameX;
        int CurrentFrameY;
        int PlayerWidth;
        int PlayerHeight;


        public bool PixelCollision(Rectangle CurrentRailRectangle, Texture2D CurrentRailTexture, Texture2D PlayerSpreadSheet, Rectangle PlayerRectangle)
        {
            Color[] ColorDataBackGround = new Color[CurrentRailRectangle.Width * CurrentRailRectangle.Height];
            Color[] ColorDataSpreadSheet = new Color[PlayerSpreadSheet.Width * PlayerSpreadSheet.Height];

            CurrentRailTexture.GetData<Color>(ColorDataBackGround);
            PlayerSpreadSheet.GetData(0, new Rectangle(CurrentFrameX * PlayerWidth, CurrentFrameY * PlayerHeight, PlayerWidth, PlayerHeight), ColorDataSpreadSheet, CurrentFrameX * CurrentFrameY, 192 * 256); //Prendre les données du frame dans le spreadsheet

            //Conscrire le rectangle de détection
            int top = Math.Max(CurrentRailRectangle.Top, PlayerRectangle.Top);
            int bottom = Math.Min(CurrentRailRectangle.Bottom, PlayerRectangle.Bottom);
            int left = Math.Max(CurrentRailRectangle.Left, PlayerRectangle.Left);
            int right = Math.Min(CurrentRailRectangle.Right, PlayerRectangle.Right);

            for (int y = top; y < bottom; y++)
            {
                for (int x = left; x < right; x++)
                {
                    Color ColorA = ColorDataBackGround[(y - CurrentRailRectangle.Top) * (CurrentRailRectangle.Width) + (x - CurrentRailRectangle.Left)];
                    Color ColorB = ColorDataSpreadSheet[(y - PlayerRectangle.Top) * (PlayerRectangle.Width) + (x - PlayerRectangle.Left)];

                    if (ColorA.A != 0 && ColorB.A != 0)
                    {

                        PlayerRectangle.Y = (y - 220);  //Point de contact avec le plancher, moins la hauteur du sprite (qui est en fait de 210)
                        return true;
                    }


                }

            }
            //playerCollisiontState = PlayerCollisionState.NoHit;
            return false;
        }

    }
}
