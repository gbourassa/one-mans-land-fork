using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace One_Man_s_Land
{
    
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        LevelTest levelTest;
       
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
            levelTest = new LevelTest();
            
        }

       
        protected override void Initialize()
        {

            //Full screen + grandeur de l'�cran
            graphics.PreferredBackBufferWidth = (1920);
            graphics.PreferredBackBufferHeight = (1080);
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            levelTest.Initialize(GraphicsDevice);
            
                       
            base.Initialize();
        }

        
        protected override void LoadContent()
        {
            
            levelTest.Load(Content);
           
            
        }

       
        protected override void UnloadContent()
        {}

        
        protected override void Update(GameTime gameTime)
        {

            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape))
                this.Exit();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();


            
            levelTest.Update(gameTime);
            
            
            base.Update(gameTime);
        }

       
        protected override void Draw(GameTime gameTime)
        {
                       
            
            levelTest.Draw(spriteBatch);
                       

            base.Draw(gameTime);
        }
    }
}
