﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class CharacterState
    {


        public enum PhysicalState
        {
            IsStandingRight,
            IsStandingLeft,
            IsWalkingRight,
            IsWalkingLeft,
            IsJumpingRight,
            IsJumpingLeft,
            IsLandingRight,
            IsLandingLeft,
            IsFallingRight,
            IsFallingLeft,
        }


        public enum AnimationState
        {
            IsStandingRight,
            IsStandingLeft,
            IsWalkingRight,
            IsWalkingLeft,
            IsJumpingRight,
            IsJumpingLeft,
            IsLandingRight,
            IsLandingLeft,
            IsFallingRight,
            IsFallingLeft,
        }

        public enum PositionState
        {
            IsOnTheGround,
            IsFalling,
            IsJumping,
        }

        public PhysicalState physicalState = new PhysicalState();
        public AnimationState animationState = new AnimationState();
        public PositionState positionState = new PositionState();

    }
}
