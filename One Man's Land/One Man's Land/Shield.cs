﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class Shield
    {

        Texture2D ShieldSpreadSheet;
        Vector2 ShieldPosition = new Vector2();
        Rectangle FrameToDraw;
        int CurrentFrameX;
        int CurrentFrameY;
        const int ShieldHeight = 296;
        const int ShieldWidth = 232;
        float AnimationTimer;
        float AnimationInterval = 30;
        float ShieldOnAnimationInterval = 80;
        Player player;

        public Shield(Player _player)
        {
            player = _player; 
                
        }


        public void Load(ContentManager content)
        {

            ShieldSpreadSheet = content.Load<Texture2D>("Shield/SpreadSheetShield");

        }


        public void Update(GameTime gameTime)
        {
            FrameToDraw = new Rectangle(CurrentFrameX * ShieldWidth, CurrentFrameY * ShieldHeight, ShieldWidth, ShieldHeight);
            
            if(player.LastStanding == "Right")
                ShieldPosition.X = player.PlayerPosition.X;

            if (player.LastStanding == "Left")
                ShieldPosition.X = player.PlayerPosition.X - 50;

            ShieldPosition.Y = player.PlayerPosition.Y;

            if (player.Shield)
            {
                if ((player.spellState == Player.SpellState.ShieldIn))
                {
                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 0;

                    if (AnimationTimer > AnimationInterval)
                    {
                        CurrentFrameX++;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX >= 9)
                        player.ShieldGrown = true;
                }

                if ((player.spellState == Player.SpellState.ShieldOut))
                {
                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 0;

                    if (AnimationTimer > AnimationInterval)
                    {
                        CurrentFrameX--;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX <= 0)
                    {
                        player.spellState = Player.SpellState.ShieldOut;
                        player.Shield = false;
                    }

                }

                if (player.spellState == Player.SpellState.ShieldOn)
                {

                    AnimationTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                    CurrentFrameY = 2;

                    if (AnimationTimer > ShieldOnAnimationInterval)
                    {
                        CurrentFrameX++;
                        AnimationTimer = 0;
                    }
                    if (CurrentFrameX > 8)
                        CurrentFrameX = 0;

                
                }


            }
        }


        public void Draw(SpriteBatch spriteBatch)
        {
            if (player.Shield)
            spriteBatch.Draw(ShieldSpreadSheet, ShieldPosition, FrameToDraw, Color.White);
            
        
        
        }

    }
}
