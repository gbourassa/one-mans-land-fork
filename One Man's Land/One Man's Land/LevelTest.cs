﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace One_Man_s_Land
{
    class LevelTest
    {

        Player player;
        Camera camera;
        Camera cameraMiddle;
        Camera cameraFar;
        Fog fog;
        Shield shield;
                     

        Texture2D FocalGround01Texture;
        Texture2D MiddleGround01Texture;
        Texture2D FarGround01Texture;
        Texture2D SkyTexture;

        Vector2 FocalGroundPosition;
        Vector2 MiddleGroundPosition;
        Vector2 FarGroundPosition;
        Vector2 SkyPosition;
        
        
        public void Initialize(GraphicsDevice graphicsDevice)
        {
            player = new Player();
            camera = new Camera(graphicsDevice.Viewport);
            cameraMiddle = new Camera(graphicsDevice.Viewport);
            cameraFar = new Camera(graphicsDevice.Viewport);
            shield = new Shield(player);
            fog = new Fog();

            player.Initialize(graphicsDevice);
            

            FocalGroundPosition = new Vector2(0);
            MiddleGroundPosition = new Vector2(0, -150);
            FarGroundPosition = new Vector2(0, -100);
            SkyPosition = new Vector2(0);

        }

        public void Load(ContentManager content)
        {

            player.Load(content);
            shield.Load(content);
            fog.Load(content);
            

            FocalGround01Texture = content.Load<Texture2D>("BackGrounds/Level01FocalGround01");
            MiddleGround01Texture = content.Load<Texture2D>("Backgrounds/Level01MiddleGround01");
            FarGround01Texture = content.Load<Texture2D>("BackGrounds/Level01FarGround01");
            SkyTexture = content.Load<Texture2D>("BackGrounds/Level01Sky01");

            
        }


        public void Update(GameTime gameTime)
        {
            player.Update(gameTime);
            shield.Update(gameTime);
            fog.Update();
            camera.Update(player.PlayerPosition, FocalGround01Texture.Width, FocalGround01Texture.Height, 1);
            cameraMiddle.Update(player.PlayerPosition, FocalGround01Texture.Width, FocalGround01Texture.Height, 0.25f);
            cameraFar.Update(player.PlayerPosition, FocalGround01Texture.Width, FocalGround01Texture.Height, 0.15f);
            

        }


        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Begin();
            spriteBatch.Draw(SkyTexture, SkyPosition, Color.White);
            spriteBatch.End();


            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cameraFar.Transform);
            spriteBatch.Draw(FarGround01Texture, FarGroundPosition, Color.White);
            spriteBatch.End();


            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, cameraMiddle.Transform);
            spriteBatch.Draw(MiddleGround01Texture, MiddleGroundPosition, Color.White);
            spriteBatch.End();


            spriteBatch.Begin();
            fog.DrawBackFog(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.Transform);
            spriteBatch.Draw(FocalGround01Texture, FocalGroundPosition, Color.White);
            player.Draw(spriteBatch);
            fog.DrawFrontFog(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive, null, null, null, null, camera.Transform);
            shield.Draw(spriteBatch);
            spriteBatch.End();

            

                       

        }




    }
}
